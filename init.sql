create database mechants;
use mechants;

DROP USER IF EXISTS seb@127.0.0.1;
CREATE USER seb@127.0.0.1 IDENTIFIED BY 'coucou';
GRANT ALL PRIVILEGES on mechants.* TO seb@127.0.0.1;

CREATE TABLE mechants (
     id INTEGER NOT NULL AUTO_INCREMENT,
     nom varchar(255),
     imgURL varchar(800),
     pouvoir varchar(255),
     PRIMARY KEY (id)
);


insert into mechants(nom, pouvoir, imgURL) values ('maxime', 'git', 'coucou');